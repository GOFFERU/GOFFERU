### Hi there 👋

<!--
**GOFFERU/GOFFERU** is a ✨ _special_ ✨ repository because its `README.md` (this file) appears on your GitHub profile.

Here are some ideas to get you started:

- 🔭 I’m currently working on IDE project.
- 🌱 I’m currently learning coding & database.
- 👯 I’m looking to collaborate on Ethereum Blockchain.
- 🤔 I’m looking for help with fully support.
- 💬 Ask me about my project.
- 📫 How to reach me: uyetar949@gmail.com
- 😄 Pronouns: YeTarOo
- ⚡ Fun fact: ...
;;
;; Domain:     yetaroo.com.
;; Exported:   2023-01-06 03:38:16
;;
;; This file is intended for use for informational and archival
;; purposes ONLY and MUST be edited before use on a production
;; DNS server.  In particular, you must:
;;   -- update the SOA record with the correct authoritative name server
;;   -- update the SOA record with the contact e-mail address information
;;   -- update the NS record(s) with the authoritative name servers for this domain.
;;
;; For further information, please consult the BIND documentation
;; located on the following website:
;;
;; http://www.isc.org/
;;
;; And RFC 1035:
;;
;; http://www.ietf.org/rfc/rfc1035.txt
;;
;; Please note that we do NOT offer technical support for any use
;; of this zone data, the BIND name server, or any other third-party
;; DNS software.
;;
;; Use at your own risk.
;; SOA Record
yetaroo.com	3600	IN	SOA	yetaroo.com root.yetaroo.com 2042464289 7200 3600 86400 3600
-->
